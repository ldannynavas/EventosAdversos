<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>

<!DOCTYPE html>
<html>
    <head>
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    </head>

    <body>
        <nav>
            <div class="nav-wrapper green accent-4">
                <a href="#!" class="brand-logo">Eventos</a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="Parametros.jsp"><i class="mdi-action-search"></i></a></li>
                    <li><a href="Registro.jsp"><i class="mdi-action-view-module"></i></a></li>
                    <li><a href="filtro.jsp"><i class="mdi-navigation-refresh"></i></a></li>
                </ul>
            </div>
        </nav>
        <div class="col s12 m12 l12">
            <img class="responsive-img" src="img/imagen1_phixr.jpg" width="100%" height="25%">
        </div>
        <%

           if (request.getParameter("msg") != null) {
        %>


        <div class="card-panel col s12 l4 m6 center-align">
            <span class="blue-text text-darken-2">

                <%
                    out.print(request.getParameter("msg"));
                %>
            </span>  
        </div>
        <% }%>
        <div class="container">
            <div class="row">

                <div class="col s6">
                    <div class="card-panel grey lighten-5 z-depth-1">
                        <div class="row valign-wrapper">
                            <div class="col s10">
                                <span class="black-text">
                                    Parametrización
                                    <a href="NuevoPais" class="btn-floating btn-large waves-effect waves-light red right" href=""><i class="mdi-content-add"></i></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s6">
                    <div class="card-panel grey lighten-5 z-depth-1">
                        <div class="row valign-wrapper">
                            <div class="col s10">
                                <span class="black-text">
                                    Eventos Adversos
                                    <a href="RegistroClinica" class="btn-floating btn-large waves-effect waves-light red right" href=""><i class="mdi-content-add"></i></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s6">
                    <div class="card-panel grey lighten-5 z-depth-1">
                        <div class="row valign-wrapper">
                            <div class="col s10">
                                <span class="black-text">
                                    Filtro
                                    <a class="btn-floating btn-large waves-effect waves-light red right" href=""><i class="mdi-content-add"></i></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
</html>
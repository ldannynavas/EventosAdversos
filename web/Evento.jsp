<%@page import="com.EventosAdversos.Dto.ParametrosDto"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>

<!DOCTYPE html>
<html>
    <head>
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    </head>

    <body>

        <nav>
            <div class="nav-wrapper green accent-4">
                <a href="#!" class="brand-logo">Eventos</a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="Parametros.jsp"><i class="mdi-action-search"></i></a></li>
                    <li><a href="Registro.jsp"><i class="mdi-action-view-module"></i></a></li>
                    <li><a href="filtro.jsp"><i class="mdi-navigation-refresh"></i></a></li>
                </ul>
            </div>
        </nav>
        <div class="col s12 m12 l12">
            <img class="responsive-img" src="img/imagen1_phixr.jpg" width="100%" height="25%">
        </div>
        <div class="container">
            <div class="row">
                <form class="col s12" method="post" action="CtrlParametro">
                    <div class="row">
                        <div class="input-field col s12">
                            <input  id="first_name" type="text" class="validate" name="Evento">
                            <label for="first_name">Evento</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="clinica">
                                <option value="" disabled selected>Seleccione una Clinica</option>

                                <option value=""></option>

                            </select>
                            <label>Asocie una clinica</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="ciudad">
                                 <option value="" disabled selected>Seleccione tipo de error</option>
                                <option value=""></option>

                            </select>
                            <label>Asocie un tipo de error</label>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="ciudad">
                                <option value="" disabled selected>Seleccione un dispositivo de software</option>

                                <option value=""></option>

                            </select>
                            <label>Asocie un dispositivo de software</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="ciudad">
                                <option value="" disabled selected>Seleccione un nivel</option>

                                <option value=""></option>

                            </select>
                            <label>Asocie un nivel</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="ciudad">
                                <option value="" disabled selected>Seleccione un tipo de lession</option>

                                <option value=""></option>

                            </select>
                            <label>Asocie un tipo de lession</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="ciudad">
                                <option value="" disabled selected>Seleccione un miembro</option>

                                <option value=""></option>

                            </select>
                            <label>Asocie un miembro</label>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit" name="reg7" value="Clinica">Registrar
                        <i class="mdi-action-done right"></i>
                    </button>
                </form>
            </div>

        </div>
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </body>
</html>
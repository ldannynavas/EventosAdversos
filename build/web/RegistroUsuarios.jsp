<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>

<!DOCTYPE html>
<html>
    <head>
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    </head>

    <body>
        <nav>
            <div class="nav-wrapper  green accent-4">
                <a href="#!" class="brand-logo">Eventos</a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="Parametros.jsp"><i class="mdi-action-search"></i></a></li>
                    <li><a href="Registro.jsp"><i class="mdi-action-view-module"></i></a></li>
                    <li><a href="filtro.jsp"><i class="mdi-navigation-refresh"></i></a></li>
                </ul>
            </div>
        </nav>
        <div class="col s12 m12 l12">
            <img class="responsive-img" src="img/imagen1_phixr.jpg" width="100%" height="25%">
        </div>
        <div class="container">
            <div class="row">
                <div class="row">
                    <form method="post" action="CtrlLogin">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="first_name" name="documento" type="text" class="validate" required>
                                <label for="first_name">Numero de identificacion</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name" type="text" name="nombre" class="validate" required>
                                <label for="last_name">Nombre</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input type="text" name="usuario" class="validate" required>
                                <label for="disabled">Usuario</label>
                            </div>
                            <div class="input-field col s6">
                                <select name="tipo" required>
                                    <option value="" disabled selected>Choose your option</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                </select>
                                <label>Tipo de usuario</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" name="password" type="password" class="validate" required>
                                <label for="password">Contraseña</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email" name="correo" class="validate" required>
                                <label for="email">Correo</label>
                            </div>
                        </div>
                        <div class="row">
                            <form class="col s12">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" name="descripcion" class="materialize-textarea" required></textarea>
                                        <label for="textarea1">Descripción</label>
                                    </div>
                                </div>

                        </div>

                        <button class="btn waves-effect waves-light" type="submit" value="registro" name="reg">Submit
                            <i class="mdi-content-send right"></i>
                        </button>


                    </form>
                </div>

            </div>
            <!--Import jQuery before materialize.js-->
            <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
            <script type="text/javascript" src="js/materialize.min.js"></script>
            <script type="text/javascript" src="js/script.js"></script>
    </body>
</html>
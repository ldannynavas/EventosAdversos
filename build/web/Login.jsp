<!DOCTYPE html>
<html>
    <head>
        <title>Inicio Sesion</title>
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/style1.css"  media="screen,projection"/>
        <meta charset="utf-8">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    </head>

    <body>
        <%

            if (request.getParameter("msg") != null) {
        %>


        <div class="card-panel col s12 l4 m6 center-align">
            <span class="blue-text text-darken-2">

                <%
                    out.print(request.getParameter("msg"));
                %>
            </span>  
        </div>
        <% }%>
        <div class="container">
            <div class="row">
                <form method="post" action="CtrlLogin">
                    <div class="col s12 m6 l6 text-aling-center center">
                        <div class="input-field col s9">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input type="text" name="user" class="validate">
                            <label for="icon_prefix">Usuario</label>
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="input-field col s9">
                            <i class="mdi-communication-phone prefix"></i>
                            <input type="password" name="password" class="validate">
                            <label for="icon_telephone">Contraseņa</label>
                        </div>
                    </div>
                    <div class="col s12 m8 l11 center">
                        <button class="btn waves-effect waves-light" type="submit" value="Login" name="ingreso">Iniciar sesion
                            <i class="mdi-content-send right"></i>
                        </button>
                    </div>
                </form>	
            </div>
        </div>

        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
      <title>Registrar Usuario</title>
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/style1.css"  media="screen,projection"/>
      <meta charset="utf-8">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    </head>

    <body>
		<div class="container">
    	<div class="row">
    	<form action="CtrlEvento" method="post">
    		<div class="col s12 m12 l4">
    			<label>Clinica</label>
				  <select>
				    <option value="" disabled selected>Seleccione una opción</option>
				    <option value="1">Option 1</option>
				    <option value="2">Option 2</option>
				    <option value="3">Option 3</option>
				  </select>
    		</div>
    		<div class="col s12 m12 l4">
    			<label>Tipo de error</label>
				  <select>
				    <option value="" disabled selected>Seleccione una opción</option>
				    <option value="1">Option 1</option>
				    <option value="2">Option 2</option>
				    <option value="3">Option 3</option>
				  </select>
    		</div>
    		<div class="col s12 m12 l4">
    			<label>Dispositivo de software asociado</label>
				  <select>
				    <option value="" disabled selected>Seleccione una opción</option>
				    <option value="1">Option 1</option>
				    <option value="2">Option 2</option>
				    <option value="3">Option 3</option>
				  </select>
    		</div>
    		<div class="col s12 m12 l4">
    			<label>Nivel del evento</label>
				  <select>
				    <option value="" disabled selected>Seleccione una opción</option>
				    <option value="1">Option 1</option>
				    <option value="2">Option 2</option>
				    <option value="3">Option 3</option>
				  </select>
    		</div>
    		<div class="col s12 m12 l4">
    			<label>Tipo de lesión</label>
				  <select>
				    <option value="" disabled selected>Seleccione una opción</option>
				    <option value="1">Option 1</option>
				    <option value="2">Option 2</option>
				    <option value="3">Option 3</option>
				  </select>
    		</div>
    		<div class="col s12 m12 l4">
    			<label>Miembro asociado</label>
				  <select>
				    <option value="" disabled selected>Seleccione una opción</option>
				    <option value="1">Option 1</option>
				    <option value="2">Option 2</option>
				    <option value="3">Option 3</option>
				  </select>
    		</div>
			  	<div class="col s12 m12 l12">
			        <div class="input-field col s12">
			          <i class="mdi-editor-mode-edit prefix"></i>
			          <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
			          <label for="icon_prefix2">Evento Adverso</label>
			        </div>
			    </div>
			    <div class="col">
		    		<button class="btn waves-effect waves-light" type="submit" name="action">Registrar
					    <i class="mdi-content-send right"></i>
					</button>
			    </div>	
			</form>
    	</div>
    </div>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
	</body>
</html>
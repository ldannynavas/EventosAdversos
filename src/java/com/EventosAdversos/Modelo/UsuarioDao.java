/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EventosAdversos.Modelo;
import com.EventoAdversos.Db.Conectar;
import com.EventosAdversos.Dto.TipoUsuarioDto;
import com.EventosAdversos.Dto.UsuarioDto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author thedarker
 */
public class UsuarioDao {
    Context context;
    Connection cnn = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    private int id;

    public UsuarioDao() {

    }
    
    public TipoUsuarioDto validarUsuario (String user, String password) throws SQLException{
        String salida;
        UsuarioDto usevali= new UsuarioDto();
        TipoUsuarioDto TUser =new  TipoUsuarioDto();
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            pstm = cnn.prepareStatement("SELECT Usuario.Usuario, Usuario.`Contraseña`, Usuario.Nombre, Usuario.correo, Usuario.Descripcion, Usuario.Tipo_Usuario_Id, Tipo_Usuario.Tipo_Usuario FROM Tipo_Usuario INNER JOIN Usuario ON Usuario.Tipo_Usuario_Id = Tipo_Usuario.Id where Usuario.Usuario = ? && Usuario.`Contraseña` = md5(?)");
            pstm.setString(1, user);
            pstm.setString(2, password);
            rs=pstm.executeQuery();
            if (rs.next()) {
                TUser.setUsuario(rs.getString("Usuario"));
                TUser.setPassword(rs.getString("Contraseña"));
                TUser.setNombre(rs.getString("Nombre"));
                TUser.setCorreo(rs.getString("correo"));
                TUser.setDescripcion(rs.getString("Descripcion"));
                TUser.setIdT(rs.getInt("Tipo_Usuario_Id"));
                TUser.setTipo(rs.getString("Tipo_Usuario"));
                id = TUser.getIdT();
                salida = "QueryOK";
                TUser.setMensaje(salida);
                
            }else{
                salida = "Datos no validos";
                TUser.setMensaje(salida);
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
        return TUser;
    }
    
    public String insertar (UsuarioDto useDto) throws  SQLException, NumberFormatException{
        
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        UsuarioDto user = new TipoUsuarioDto();
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Usuario` (`Id`, `Usuario`, `Contraseña`, `Nombre`, `correo`, `Descripcion`, `Tipo_Usuario_Id`) VALUES (?, ?, md5(?), ?, ?, ?, ?);");
            pstm.setInt(1, useDto.getId());
            pstm.setString(2, useDto.getUsuario());
            pstm.setString(3, useDto.getPassword());
            pstm.setString(4, useDto.getNombre());
            pstm.setString(5, useDto.getCorreo());
            pstm.setString(6, useDto.getDescripcion());
            pstm.setInt(7, useDto.getTipoId());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro creado";
            }else{
                salida = "Error en el registro";
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        }catch (NumberFormatException num){
            salida = num.getMessage();
        }
        
        return salida;
        
    }
       
    }
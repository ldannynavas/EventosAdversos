/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EventosAdversos.Modelo;

import com.EventoAdversos.Db.Conectar;
import com.EventosAdversos.Dto.ParametrosDto;
import com.EventosAdversos.Dto.TipoUsuarioDto;
import com.EventosAdversos.Dto.UsuarioDto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author thedarker
 */
public class ParametroDao {
    
    Context context;
    Connection cnn=null;
    PreparedStatement pstm=null;
    ResultSet rs=null;
    private int id;

    public ParametroDao() {
    }
    
    
    public String insertaC (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        UsuarioDto user = new TipoUsuarioDto();
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Ciudad` (`Id`, `Ciudad`, `Pais_Id`) VALUES (NULL, ?, ?);");
            pstm.setString(1, paradto.getCiudad());
            pstm.setInt(2, paradto.getIdPais());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        }catch(NumberFormatException num){
            salida = num.getMessage();
        }
        
        return salida;
        
    }
    public String insertaP (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Pais` (`Id`, `Pais`) VALUES (NULL, ?)");
            pstm.setString(1, paradto.getPais());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        }catch(NumberFormatException num){
            salida = num.getMessage();
        }
        
        return salida;
        
    }
    public String insertaCl (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        UsuarioDto user = new TipoUsuarioDto();
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Ciudad` (`Id`, `Ciudad`, `Pais_Id`) VALUES (NULL, ?, NULL);");
            pstm.setString(1, paradto.getCiudad());
            pstm .setString(2, paradto.getDescripcion());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        }catch(NumberFormatException num){
            salida = num.getMessage();
        }
        
        return salida;
        
    }
    public String insertaDs (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        UsuarioDto user = new TipoUsuarioDto();
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Dispositivo_Software_Asociado` (`Id`, `Nombre_Dispositivo`, `Descripcion_Dispositivo`) VALUES (NULL, ?, ?);");
            pstm.setString(1, paradto.getCiudad());
            pstm .setString(2, paradto.getDescripcion());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        }catch(NumberFormatException num){
            salida = num.getMessage();
        }
        
        return salida;
        
    }
    public String insertaMAs (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Miembro_asociado_Evento` (`Id`, `Nombre_Miembro`, `Descripcion_Miembro`) VALUES (NULL, ?, ?);");
            pstm.setString(1, paradto.getMiembroAsociado());
            pstm .setString(2, paradto.getDescripcion());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        }catch(NumberFormatException num){
            salida = num.getMessage();
        }
        
        return salida;
        
    }
    public String insertaNivel (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        UsuarioDto user = new TipoUsuarioDto();
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Nivel_Evento_Adverso` (`Id`, `Nombre_Nivel`, `Descripcion_Nivel`) VALUES (NULL, ?, ?)");
            pstm.setString(1, paradto.getNivelVento());
            pstm .setString(2, paradto.getDescripcion());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        }catch(NumberFormatException num){
            salida = num.getMessage();
        }
        
        return salida;
        
    }
    public String insertaError (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Tipo_Error_Evento` (`Id`, `Nombre`, `Descripcion`) VALUES (NULL, ?, ?)");
            pstm.setString(1, paradto.getTipoError());
            pstm .setString(2, paradto.getDescripcion());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException ex) {
            salida = ex.getMessage();
        }catch(NumberFormatException num){
            salida = num.getMessage();
        }
        
        return salida;
        
    }
    public String insertaLesion (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Tipo_Lesion` (`Id`, `Nombre_Lesion`, `Descripcion_Lesion`) VALUES (NULL, ?, ?)");
            pstm.setString(1, paradto.getTipoLession());
            pstm .setString(2, paradto.getDescripcion());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException | NumberFormatException ex) {
            salida = ex.getMessage();
        }
        
        return salida;
        
    }
    public String insertaClinica (ParametrosDto paradto) throws SQLException, NumberFormatException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        String salida;
        int resultado;
        
        try {
            pstm = cnn.prepareStatement("INSERT INTO `Eventos`.`Clinica` (`Id`, `Nombre_Clinica`, `Descripcion_Clinica`, `Direccion`, `Ciudad_Id`) VALUES (NULL, ?, ?, ?, ?);");
            pstm.setString(1, paradto.getClinica());
            pstm .setString(2, paradto.getDescripcion());
            pstm.setString(3, paradto.getDireccion());
            pstm.setInt(4, paradto.getIdCiudad());
            resultado = pstm.executeUpdate();
            if (resultado>0) {
                salida = "Registro Creado";
            } else {
                salida = "Error en el registro";
            }
        } catch (SQLException | NumberFormatException ex) {
            salida = ex.getMessage();
        }
        
        return salida;
        
    }
     public ArrayList ListarPais() throws ArrayIndexOutOfBoundsException, ArrayStoreException, SQLException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         ArrayList Lpais =new ArrayList();
         
         try{
             pstm=cnn.prepareStatement("SELECT Pais.Id , Pais.Pais FROM Pais");
             rs=pstm.executeQuery();
         while ( rs.next()){
             ParametrosDto ParDto = new ParametrosDto();
             ParDto.setId(rs.getInt(1));
             ParDto.setPais(rs.getString(2));
             Lpais.add(ParDto);
         }
                         
         }catch(SQLException ex){
             throw ex;
         }finally{
             if (rs!=null){
             rs.close();
         }
             if(pstm !=null){
                 pstm.close();
             }
             if(cnn !=null){
                 cnn.close();
             }
                 
         }
         
         return Lpais;
     }
     public ArrayList ListarCiudad() throws ArrayIndexOutOfBoundsException, ArrayStoreException, SQLException{
        try {
            cnn = Conectar.getInstace();
        } catch (NamingException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         ArrayList Lciudad =new ArrayList();
         
         try{
             pstm=cnn.prepareStatement("SELECT `Id`, `Ciudad` FROM `Ciudad`");
             rs=pstm.executeQuery();
         while ( rs.next()){
             ParametrosDto ParDto = new ParametrosDto();
             ParDto.setId(rs.getInt(1));
             ParDto.setCiudad(rs.getString(2));
             Lciudad.add(ParDto);
         }
                         
         }catch(SQLException ex){
             throw ex;
         }finally{
             if (rs!=null){
             rs.close();
         }
             if(pstm !=null){
                 pstm.close();
             }
             if(cnn !=null){
                 cnn.close();
             }
                 
         }
         
         return Lciudad;
     }
}

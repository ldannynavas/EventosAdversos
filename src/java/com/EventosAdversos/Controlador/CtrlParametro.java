/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EventosAdversos.Controlador;

import com.EventosAdversos.Dto.ParametrosDto;
import com.EventosAdversos.Modelo.ParametroDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author thedarker
 */
public class CtrlParametro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            ParametroDao paradao = new ParametroDao();
            ParametrosDto paradto = new ParametrosDto();
            
            
            if (request.getParameter("reg")!=null && request.getParameter("reg").equals("Ciudad")) {
                paradto.setCiudad(request.getParameter("ciudad"));
                paradto.setIdPais(Integer.parseInt(request.getParameter("pais")));
                try {
                    String salida1 = paradao.insertaC(paradto);
                    response.sendRedirect("panel.jsp?msg="+salida1);
                } catch (SQLException ex) {
                    response.sendRedirect("Cuidades.jsp?msg="+ex.getMessage());
                }
            }else if (request.getParameter("reg1")!=null && request.getParameter("reg1").equals("pais")) {
                paradto.setPais(request.getParameter("pais"));
                try {
                    String salida1 = paradao.insertaP(paradto);
                    response.sendRedirect("panel.jsp?msg="+salida1);
                } catch (SQLException ex) {
                    response.sendRedirect("Cuidades.jsp?msg="+ex.getMessage());
                }
            }else if (request.getParameter("reg2")!=null && request.getParameter("reg2").equals("registroDs")) {
                paradto.setCiudad(request.getParameter("dispositivo"));
                paradto.setDescripcion(request.getParameter("descripcion"));
                try {
                    String salida1 = paradao.insertaDs(paradto);
                    response.sendRedirect("panel.jsp?msg="+salida1);
                } catch (SQLException ex) {
                    response.sendRedirect("Cuidades.jsp?msg="+ex.getMessage());
                }
            }else if (request.getParameter("reg3")!=null && request.getParameter("reg3").equals("registroMAs")) {
                paradto.setMiembroAsociado(request.getParameter("miembro"));
                paradto.setDescripcion(request.getParameter("descripcion"));
                try {
                    String salida1 = paradao.insertaMAs(paradto);
                    response.sendRedirect("panel.jsp?msg="+salida1);
                } catch (SQLException ex) {
                    response.sendRedirect("Cuidades.jsp?msg="+ex.getMessage());
                }
            }else if (request.getParameter("reg4")!=null && request.getParameter("reg4").equals("Nivel")) {
                paradto.setNivelVento(request.getParameter("nivel"));
                paradto.setDescripcion(request.getParameter("descripcion"));
                try {
                    String salida1 = paradao.insertaNivel(paradto);
                    response.sendRedirect("panel.jsp?msg="+salida1);
                } catch (SQLException ex) {
                    response.sendRedirect("Cuidades.jsp?msg="+ex.getMessage());
                }
            }else if (request.getParameter("reg5")!=null && request.getParameter("reg5").equals("Error")) {
                paradto.setTipoError(request.getParameter("error"));
                paradto.setDescripcion(request.getParameter("descripcion"));
                try {
                    String salida1 = paradao.insertaError(paradto);
                    response.sendRedirect("panel.jsp?msg="+salida1);
                } catch (SQLException ex) {
                    response.sendRedirect("Cuidades.jsp?msg="+ex.getMessage());
                }
            }else if (request.getParameter("reg6")!=null && request.getParameter("reg6").equals("Lesion")) {
                paradto.setTipoLession(request.getParameter("lesion"));
                paradto.setDescripcion(request.getParameter("descripcion"));
                try {
                    String salida1 = paradao.insertaLesion(paradto);
                    response.sendRedirect("panel.jsp?msg="+salida1);
                } catch (SQLException ex) {
                    response.sendRedirect("Cuidades.jsp?msg="+ex.getMessage());
                }
            }else if (request.getParameter("reg7")!=null && request.getParameter("reg7").equals("Clinica")) {
                paradto.setClinica(request.getParameter("clinica"));
                paradto.setDescripcion(request.getParameter("descripcion"));
                paradto.setDireccion(request.getParameter("direccion"));
                paradto.setIdCiudad(Integer.parseInt(request.getParameter("ciudad")));
                
                try {
                    String salida1 = paradao.insertaClinica(paradto);
                    response.sendRedirect("panel.jsp?msg="+salida1);
                } catch (SQLException ex) {
                    response.sendRedirect("Cuidades.jsp?msg="+ex.getMessage());
                }
            }
            
            
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CtrlParametro</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CtrlParametro at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

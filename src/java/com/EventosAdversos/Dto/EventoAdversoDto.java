/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EventosAdversos.Dto;

/**
 *
 * @author thedarker
 */
public class EventoAdversoDto {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvento() {
        return Evento;
    }

    public void setEvento(String Evento) {
        this.Evento = Evento;
    }

    public int getIdClinica() {
        return idClinica;
    }

    public void setIdClinica(int idClinica) {
        this.idClinica = idClinica;
    }

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }

    public int getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(int idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public int getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(int idNivel) {
        this.idNivel = idNivel;
    }

    public int getIdLession() {
        return idLession;
    }

    public void setIdLession(int idLession) {
        this.idLession = idLession;
    }

    public int getIdMiembroAsociado() {
        return idMiembroAsociado;
    }

    public void setIdMiembroAsociado(int idMiembroAsociado) {
        this.idMiembroAsociado = idMiembroAsociado;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }
    
    private int id;
    private String Evento;
    private int idClinica;
    private int idError;
    private int idDispositivo;
    private int idNivel;
    private int idLession;
    private int idMiembroAsociado;
    private int idusuario;
    
}

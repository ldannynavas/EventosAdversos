/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EventosAdversos.Dto;

/**
 *
 * @author thedarker
 */
public class ParametrosDto {

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getClinica() {
        return clinica;
    }

    public void setClinica(String clinica) {
        this.clinica = clinica;
    }

    public String getDispositivoSw() {
        return dispositivoSw;
    }

    public void setDispositivoSw(String dispositivoSw) {
        this.dispositivoSw = dispositivoSw;
    }

    public String getMiembroAsociado() {
        return miembroAsociado;
    }

    public void setMiembroAsociado(String miembroAsociado) {
        this.miembroAsociado = miembroAsociado;
    }

    public String getNivelVento() {
        return nivelVento;
    }

    public void setNivelVento(String nivelVento) {
        this.nivelVento = nivelVento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTipoError() {
        return tipoError;
    }

    public void setTipoError(String tipoError) {
        this.tipoError = tipoError;
    }

    public String getTipoLession() {
        return tipoLession;
    }

    public void setTipoLession(String tipoLession) {
        this.tipoLession = tipoLession;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;
    private String ciudad;
    private String clinica;
    private String dispositivoSw;
    private String miembroAsociado;
    private String nivelVento;
    private String pais;
    private String tipoError;
    private String tipoLession;

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    private String direccion; 

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    private String descripcion;

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }
    private int  idPais;

    public int getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        this.idCiudad = idCiudad;
    }
    private int idCiudad;
    
}
